package com.gamedev.demotask.ws;

import com.gamedev.demotask.domain.GameCharacter;
import com.gamedev.demotask.domain.CharacterRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;

@RestController
public class PlayerStatisticController {
    private final CharacterRepository characterRepository;

    public PlayerStatisticController(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @GetMapping("/alive")
    public String imAlive() {
        return String.format("I'm alive %s", LocalDateTime.now());
    }

    @GetMapping("/characters/{id}")
    public GameCharacter getCharacter(@PathVariable Integer id) {
        validateEntityExists(id);

        return characterRepository.getGameCharacter(id);
    }

    @PostMapping("/characters/{id}/addExp")
    public ResponseEntity<GameCharacter> updateCharacterExp(@PathVariable Integer id, @RequestParam Integer expToAdd ) {
        validateEntityExists(id);

        GameCharacter updateCharacter = characterRepository.updateCharacterWithExperience(id, expToAdd);
        return new ResponseEntity<>(updateCharacter, HttpStatus.OK);
    }

    private void validateEntityExists(@PathVariable Integer id) {
        if (!characterRepository.isEntityPresentInMap(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
