package com.gamedev.demotask.app.configuration;

import com.gamedev.demotask.levelUpConfiguration.LevelUpConfiguration;
import com.gamedev.demotask.levelUpConfiguration.LevelUpConfigurationValidator;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
@EnableConfigurationProperties(LevelUpConfiguration.class)
public class Configuration {

    @Bean
    public static LevelUpConfigurationValidator configurationPropertiesValidator() {
        return new LevelUpConfigurationValidator();
    }
}
