package com.gamedev.demotask.domain;

import com.gamedev.demotask.levelUpConfiguration.LevelUpConfiguration;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Repository
public class CharacterRepository {

    private final LevelUpConfiguration levelUpConfiguration;
    private ConcurrentMap<Integer, GameCharacter> repository = new ConcurrentHashMap<>();

    {
        repository.put(1, new GameCharacter(1));
        repository.put(2, new GameCharacter(2));
        repository.put(3, new GameCharacter(3));
        repository.put(4, new GameCharacter(4));
        repository.put(5, new GameCharacter(5));
        repository.put(6, new GameCharacter(6));
        repository.put(7, new GameCharacter(7));
        repository.put(8, new GameCharacter(8));
        repository.put(9, new GameCharacter(9));
        repository.put(10, new GameCharacter(10));
    }

    public CharacterRepository(LevelUpConfiguration levelUpConfiguration) {
        this.levelUpConfiguration = levelUpConfiguration;
    }

    public GameCharacter getGameCharacter(Integer id) {
        return repository.get(id);
    }

    public boolean isEntityPresentInMap(Integer id) {
        return repository.containsKey(id);
    }

    public GameCharacter updateCharacterWithExperience(Integer key, Integer experience) {
        GameCharacter updatedCharacter = repository.computeIfPresent(key, (integer, character) -> {
            Integer characterExp = character.getExp();
            Integer characterLevel = character.getLevel();

            characterExp += experience;

            if (maxLevelAchieved(characterLevel)) {
                character.setExp(characterExp);
                return character;
            }

            increaseCharacterLevelWhileEnoughExp(character, characterExp, characterLevel);

            return character;
        });

        return updatedCharacter;
    }

    private void increaseCharacterLevelWhileEnoughExp(GameCharacter character, Integer characterExp, Integer characterLevel) {
        Integer expRequiredForLevelUp = expToIncreaseToNextLevel(characterLevel);

        while (characterExp >= expRequiredForLevelUp) {
            characterLevel++;
            characterExp -= expRequiredForLevelUp;
            if (maxLevelAchieved(characterLevel)) {
                break;
            }
            expRequiredForLevelUp = expToIncreaseToNextLevel(characterLevel);
        }

        character.setLevel(characterLevel);
        character.setExp(characterExp);
    }

    public boolean maxLevelAchieved(Integer currentLevel) {
        return currentLevel == levelUpConfiguration.getMaxLevel();
    }

    public Integer expToIncreaseToNextLevel(Integer currentLevel) {
        return levelUpConfiguration.getLevelUpRanges().stream()
                .filter(range -> range.getFrom() <= currentLevel && range.getTo() >= currentLevel)
                .findFirst()
                .map(range -> range.getExpToNextLevel()).get();
    }
}
