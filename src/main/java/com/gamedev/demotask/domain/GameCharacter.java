package com.gamedev.demotask.domain;

public class GameCharacter {
    private final Integer id;
    private Integer level = 1;
    private Integer exp = 0;

    public GameCharacter(Integer id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }
}
