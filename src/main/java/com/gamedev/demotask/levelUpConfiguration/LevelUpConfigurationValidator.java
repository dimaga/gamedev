package com.gamedev.demotask.levelUpConfiguration;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Comparator;
import java.util.stream.Collectors;

public class LevelUpConfigurationValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return LevelUpConfiguration.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        LevelUpConfiguration levelUpConfiguration = (LevelUpConfiguration) target;

        validateMaxLevelIsSet(errors, levelUpConfiguration);
        validateExpToNextLevelIsSetForAllLevelUpRanges(errors, levelUpConfiguration);
        validateFromAndToBoundsAreSetForAllLevelUpRanges(errors, levelUpConfiguration);

        if (!errors.hasErrors()) {
            levelUpConfiguration.getLevelUpRanges().sort(Comparator.comparingInt(LevelUpRange::getFrom));

            validateNoGapBetweenLastLevelUpRangeAndMaxLevel(errors, levelUpConfiguration);
            validateProperlyConfiguredLevelUpRangesBounds(errors, levelUpConfiguration);
        }

    }

    private void validateProperlyConfiguredLevelUpRangesBounds(Errors errors, LevelUpConfiguration levelUpConfiguration) {
        Integer levelRangesSum = levelUpConfiguration.getLevelUpRanges().stream().reduce(0, (a, levelUpRange) -> a + (levelUpRange.getTo() - levelUpRange.getFrom() + 1), (a, b) -> a + b);
        if (levelRangesSum != levelUpConfiguration.getMaxLevel() - 1) {
            String allLevelUpRanges = levelUpConfiguration.getLevelUpRanges().stream().map(LevelUpRange::toString).collect(Collectors.joining("\n\t\t"));
            errors.reject("notProperlyConfiguredLevelUpRangesBounds", String.format("Level up ranges bounds configured incorrectly. Please check it: \n\t\t%s", allLevelUpRanges ));
        }
    }

    private void validateNoGapBetweenLastLevelUpRangeAndMaxLevel(Errors errors, LevelUpConfiguration levelUpConfiguration) {
        if (haveGapBetweenLastLevelUpRangeAndMaxLevel(levelUpConfiguration)) {
            LevelUpRange lastLevelUpRange = levelUpConfiguration.getLevelUpRanges().get(levelUpConfiguration.getLevelUpRanges().size() - 1);
            errors.reject("gapBetweenMaxLevelUpRangeAndMaxLevel", String.format("There is gap between max level up range %s and max level %s", lastLevelUpRange, levelUpConfiguration.getMaxLevel()));
        }
    }

    private void validateFromAndToBoundsAreSetForAllLevelUpRanges(Errors errors, LevelUpConfiguration levelUpConfiguration) {
        levelUpConfiguration.getLevelUpRanges().stream()
                .filter(levelUpRange -> levelUpRange.getFrom() == null || levelUpRange.getTo() == null)
                .forEach(levelUpRange -> errors.reject("lowOrUpperBoundOfRangeNotSet", String.format("Low or upper bound not set for range %s", levelUpRange)));
    }

    private void validateExpToNextLevelIsSetForAllLevelUpRanges(Errors errors, LevelUpConfiguration levelUpConfiguration) {
        levelUpConfiguration.getLevelUpRanges()
                .stream()
                .filter(levelUpRange -> levelUpRange.getExpToNextLevel() == null)
                .forEach(levelUpRange -> errors.reject("expToNextLevelNotDefined", String.format("ExpToNextLevel is not defined for range %s", levelUpRange)));
    }

    private void validateMaxLevelIsSet(Errors errors, LevelUpConfiguration levelUpConfiguration) {
        if (levelUpConfiguration.getMaxLevel() == null) {
            errors.reject("maxLevelNotSet", "Max level property not set.");
        }
    }

    private boolean haveGapBetweenLastLevelUpRangeAndMaxLevel(LevelUpConfiguration levelUpConfiguration) {
        LevelUpRange lastLevelUpRange = levelUpConfiguration.getLevelUpRanges().get(levelUpConfiguration.getLevelUpRanges().size() - 1);
        return lastLevelUpRange.getTo() + 1 != levelUpConfiguration.getMaxLevel();
    }
}
