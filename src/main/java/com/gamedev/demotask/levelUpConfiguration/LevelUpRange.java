package com.gamedev.demotask.levelUpConfiguration;

public class LevelUpRange {

    private Integer from;
    private Integer to;
    private Integer expToNextLevel;

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public Integer getExpToNextLevel() {
        return expToNextLevel;
    }

    public void setExpToNextLevel(Integer expToNextLevel) {
        this.expToNextLevel = expToNextLevel;
    }

    @Override
    public String toString() {
        return "LevelUpRange{" +
                "from=" + from +
                ", to=" + to +
                ", expToNextLevel=" + expToNextLevel +
                '}';
    }
}
