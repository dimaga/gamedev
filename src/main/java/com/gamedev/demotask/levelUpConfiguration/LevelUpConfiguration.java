package com.gamedev.demotask.levelUpConfiguration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "app.properties.level-up")
@Validated
public class LevelUpConfiguration {
    private List<LevelUpRange> levelUpRanges = new ArrayList<>();
    private Integer maxLevel;

    public List<LevelUpRange> getLevelUpRanges() {
        return levelUpRanges;
    }

    public void setLevelUpRanges(List<LevelUpRange> levelUpRanges) {
        this.levelUpRanges = levelUpRanges;
    }

    public Integer getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(Integer maxLevel) {
        this.maxLevel = maxLevel;
    }
}
